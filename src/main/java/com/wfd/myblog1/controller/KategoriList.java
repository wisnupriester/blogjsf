/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wfd.myblog1.controller;

import com.wfd.myblog1.entity.Kategori;
import com.wfd.myblog1.service.KategoriService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Dynabook
 */

@Named
@ViewScoped
public class KategoriList implements Serializable {
    
    @EJB
    private KategoriService kategoriService;
    
    private List<Kategori> kategoris;
    
    private String filterText;
    private int page;
    private int limit;
    
    @PostConstruct
    public void init() {
        filterText = "";
        load();
    }
    
    public void load() {
        kategoris = kategoriService.findAll();
        
    }
    
    
    public void search() {
        load();
    }

    public String getFilterText() {
        return filterText;
    }

    public void setFilterText(String filterText) {
        this.filterText = filterText;
    }

    public List<Kategori> getKategoris() {
        return kategoris;
    }

    public void setKategoris(List<Kategori> kategoris) {
        this.kategoris = kategoris;
    }
    
    
    
}
