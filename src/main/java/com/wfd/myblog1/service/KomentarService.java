/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.wfd.myblog1.service;

import com.wfd.myblog1.entity.Artikel;
import com.wfd.myblog1.entity.Komentar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Dynabook
 */
@Stateless
public class KomentarService {
    
    @PersistenceContext(unitName = "myblogsPU")
    private EntityManager em;
    
    public Komentar findbyId(String id) {
        return em.find(Komentar.class, id);        
    }
    
    public List<Komentar> findAll() {
        return em.createQuery("Select k FROM Komentar k ORDER BY k.artikel ASC")
                .getResultList();
    }
    
    public List<Komentar> findByArtikel(Artikel artikel) {
        return em.createQuery("Select k FROM Komentar k WHERE k.artikel = :artikel ORDER BY k.waktuDibuat DESC")
                .getResultList();
    }
    
    public void create(Komentar komentar){
        komentar.setId(UUID.randomUUID().toString());
        komentar.setWaktuDibuat(new Date());
        em.persist(komentar);        
    }
    
    public void edit(Komentar komentar){
        em.merge(komentar);
    }
    
    public void remove(Komentar komentar){
        em.remove(komentar);
    }
}
